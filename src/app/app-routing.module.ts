// Imports
// Deprecated import
// import { provideRouter, RouterConfig } from '@angular/router';
import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent }    from './login.component';
import { SignupComponent }    from './signup.component';
import { NgModule }              from '@angular/core';

// Route Configuration
export const approutes: Routes = [
  { path: '', component: AppComponent },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent }
];


@NgModule({
  imports: [
    RouterModule.forRoot(approutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}