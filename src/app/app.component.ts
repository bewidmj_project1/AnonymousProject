import { Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'my-app',
  templateUrl: './appcomponent.html',
})
export class AppComponent  {
  constructor( private route: ActivatedRoute,
    private router: Router
){
     this.router.navigate(['/login']);
  }
 }
